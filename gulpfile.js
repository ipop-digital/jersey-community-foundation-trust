"use strict";


const { dest, pipe, series, src, parallel, watch } = require("gulp");

const uglify = require("gulp-uglify");
const pump = require("pump");
const rename = require("gulp-rename");
const babel = require("gulp-babel");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const jade = require("gulp-jade");
const bs = require("browser-sync").create();
const concat = require('gulp-concat');

const enableBrowserSync = true; // | false
let browserSyncInitialised = false;

function browserSyncInit() {
  if (enableBrowserSync) {
    bs.init({
      proxy: "http://localhost/indigo-medical/web"
    });
    browserSyncInitialised = true;
  }
}

function browserSyncReload(done) {
  bs.reload();
  done();
}

function compileJs(cb) {
  pump(
    [
      // src(["src/js/*.js", "src/js/custom/*.js"]),

      //1:
      //Switch this line out for the line above when going live.
      src(["src/js/main.js"]),

      babel({
        presets: ["@babel/preset-env"],
        compact: true
      }),

      //2: 
      //Add back in 'concat('all.js') prior to going live & comment out all references to the JS files from within the footer other than all.min.js.
      //concat('all.js'),
      uglify(),
      // rename({ suffix: ".min" }),
      dest("web/build/js")
    ],
    cb
  );
}

function compileJadeTemplates() {
  return src("templates/**/*.jade")
    .pipe(
      jade({
        pretty: false
      })
    )
    .on("error", function (err) {
      console.log(err.toString());

      this.emit("end");
    })

    .pipe(dest("templates/"));
}

function transpileCss() {
  return src("src/sass/*.scss")
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"],
        cascade: false
      })
    )
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    // .pipe(rename({ suffix: ".min" }))
    .pipe(dest("web/build/css"));
}

function watchFiles() {
  watch("src/sass/**/*.scss", transpileCss);
  watch("src/js/**/*.js", compileJs);
  watch("templates/**/*.jade", series(compileJadeTemplates, browserSyncReload))
}

exports.compileJs = series(compileJs);
exports.compileJadeTemplates = series(compileJadeTemplates);
exports.buildSass = series(transpileCss);
exports.watchFiles = series(watchFiles);

exports.default = series(
  parallel(compileJs, compileJadeTemplates, transpileCss),
  watchFiles
);

exports.dev = series(
  parallel(compileJs, compileJadeTemplates, transpileCss),
  parallel(browserSyncInit, watchFiles)
);
