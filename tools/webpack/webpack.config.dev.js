const merge = require('webpack-merge');
var path = require('path');
const common = require('./webpack.config.common.js');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map'
});