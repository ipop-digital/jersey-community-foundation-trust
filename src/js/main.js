function initAccordian() {
    
    // get all accordian boxes
    var accordians = document.querySelectorAll(".accordian .box");

    accordians.forEach( function(accordian) {

        var title = accordian.querySelector("h2");
        if (!title) return;
        title.addEventListener("click", function() {
            if (accordian.classList.contains("active")) {
                // deactivate
                accordian.classList.remove("active");
                accordian.querySelector('i').classList.remove('fa-minus')
                accordian.querySelector('i').classList.add('fa-plus');

                accordian.querySelector('.blurb').removeAttribute('hidden');
                accordian.querySelector('.content').setAttribute('hidden', "true");
            } else {
                // activate
                accordian.classList.add("active");
                accordian.querySelector('i').classList.remove('fa-plus');
                accordian.querySelector('i').classList.add('fa-minus');

                accordian.querySelector(".blurb").setAttribute("hidden", "true");
                accordian.querySelector('.content').removeAttribute('hidden');
            }
        });
    });

}

function getSlideTitles(s) {

 // get the previous slide title
 if ( s.progress > 0) {

    var slide = s.slides[s.progress - 1];
    if (slide){ 
        var text = slide.querySelector('h2').innerText;

        if (text) {
            // set the previous button to have the text value otherwise read "Previous";
            s.navigation.prevEl.querySelector('p').childNodes[0].nodeValue = text;
        } else {
            s.navigation.prevEl.querySelector('p').childNodes[0].nodeValue = 'Previous';
        }
    }
 }


 // get the next slide title
 if ( s.progress < s.slides.length) {

    var slide = s.slides[s.progress + 1];
    if ( slide ) {
        var text = slide.querySelector('h2').innerText;

        if (text) {
            // set the previous button to have the text value otherwise read "Previous";
            s.navigation.nextEl.querySelector('p').childNodes[0].nodeValue = text;
        } else {
            s.navigation.nextEl.querySelector('p').childNodes[0].nodeValue = 'Next';
        }
    }

 }

}

function initSwiper() {

    var swipers = document.querySelectorAll(".swiper-container:not(.swiper-navigation)");

    var thumbs = document.querySelector(".swiper-navigation");
    var thumbSwiper = null;
    if (thumbs) {
        thumbSwiper = new Swiper(thumbs, {
            slidesPerView: 4,
            spaceBetween: 10,
            grabCursor: true,
            navigation: {
                nextEl: thumbs.closest('.relative') ? thumbs.closest('.relative').querySelector('.next') : null,
                prevEl: thumbs.closest('.relative') ? thumbs.closest('.relative').querySelector('.prev') : null,
            },
            breakpoints: {
                // when window width is >= 320px
                320: {
                  slidesPerView: 2,
                  spaceBetween: 20
                },
                // when window width is >= 640px
                1080: {
                  slidesPerView: 3,
                  spaceBetween: 40
                }
              }
        });
    }
    

    swipers.forEach( function(swiper) {

        var slidePerView = swiper.dataset.slidesperview | 1;   
        
        var s = new Swiper(swiper, {
            slidesPerView: slidePerView,
            spaceBetween: 30,
            grabCursor: true,
            updateOnWindowResize: true,
            autoHeight: true,
            navigation: {
                nextEl: swiper.closest('.swiper-content') ? swiper.closest('.swiper-content').querySelector('.next') : null,
                prevEl: swiper.closest('.swiper-content') ? swiper.closest('.swiper-content').querySelector('.prev') : null,
            },
            thumbs: {
                swiper: thumbSwiper
            },
            init: false
        });

        if (swiper.dataset.noslidechange === "true") {

            s.on('slideChange', function() { 
                getSlideTitles(this);
            });
    
            s.on('init', function() {
                getSlideTitles(this);         
            });

        } else {

            function basicNavChange(s) {
                // get current slide
                // get previous slide
                var prev = s.slides[s.activeIndex - 1]?.dataset.navtitle;
                console.log({prev});
    
                // replace the text 
                if (prev) {
                    var el = document.querySelector('.swiper-content').querySelector('.prev');
                    if (el) {
                        el.querySelector('p').childNodes[0].nodeValue = prev;
                    }
                }
    
                // get next slide
                var next = s.slides[s.activeIndex + 1]?.dataset.navtitle;
                console.log(next);
                if (next) {
                    var el = document.querySelector('.swiper-content').querySelector('.next');
                    if (el) {
                        el.querySelector('p').childNodes[0].nodeValue = next;
                    }
                }
            }


            s.on("init", function() {
                var self = this;
                basicNavChange(self);
            })
            s.on("slideChange", function() {
                var self = this;
                basicNavChange(self);   
            })
        }

        
        
        s.init();

    });

}

function equalHeights(className) {
    var findClass = document.querySelectorAll(className);
    var tallest = 0; 
    // Loop over matching divs
    findClass.forEach( function(ele, index) {

        // remove the current height
        ele.style.height = "auto";

        var eleHeight = ele.offsetHeight;
        tallest = (eleHeight>tallest ? eleHeight : tallest); /* look up ternary operator if you dont know what this is */
    })

    findClass.forEach( function(el) {
        el.style.height = tallest + "px";
    })
}


var members = document.querySelectorAll(".member");
var overlay = document.querySelector("#overlay");
var memberHighlight = document.querySelector("#memberHighlight");
var close = document.querySelector("#memberHighlight .close");

function showMember(data) {
    overlay.classList.add("active");
    document.body.classList.add("overlay");
    // show the member
    memberHighlight.classList.add("active");

    memberHighlight.querySelector("#name").innerText = data.name;
    memberHighlight.querySelector("#position").innerText = data.position;
    memberHighlight.querySelector("#image").src = data.image;
    memberHighlight.querySelector("#content").innerText = "";
    memberHighlight.querySelector("#content").insertAdjacentHTML( 'beforeend',  data.content );


}

function hideMember() {
    overlay.classList.remove("active");
    document.body.classList.remove("overlay");
    // show the member
    memberHighlight.classList.remove("active");
}

function initTeam() {

    // temp data
    var tmpMember = {
        name: "",
        position: "",
        image: "",
        content: ""
    };

    members.forEach( function(staff){
        staff.addEventListener("click", function() {

           // grab data
           tmpMember.name = this.dataset.name;
           tmpMember.position = this.dataset.position;
           tmpMember.image = this.dataset.image;
           tmpMember.content = this.dataset.content;

           showMember(tmpMember);

        });
    });
    if ( overlay ){
        overlay.addEventListener("click", function() {
            hideMember();
    
            // clear tmpMember
    
            tmpMember = {
                name: "",
                position: "",
                image: "",
                content: ""
            }; 
    
        });
    
        close.addEventListener("click", function() {
            hideMember();
    
            tmpMember = {
                name: "",
                position: "",
                image: "",
                content: ""
            }; 
        });
    }
    

}

function initHeader() { 

    var header = document.querySelector("#header");
    var open = header.querySelector(".open");
    var close = header.querySelector(".close");
    var nav = header.querySelector("nav");

    // get all nav items with children
    var navLinksWithChildren = header.querySelectorAll(".has-children");

    open.addEventListener("click", function() {
        this.classList.toggle("active");
        close.classList.toggle("active");
        nav.classList.add("active");
    });

    close.addEventListener("click", function() {
        this.classList.toggle("active");
        open.classList.toggle("active");
        nav.classList.remove("active");
    });


    navLinksWithChildren.forEach( function(link) {
        
        link.addEventListener("click", function(e) {

            if (window.innerWidth < 1050) {

                // close all others
                for (var i = 0; i < navLinksWithChildren.length; i++) {
                    if ( link !== navLinksWithChildren[i] ) {
                        navLinksWithChildren[i].classList.remove("active");
                    }
                }
                
                // if you have clicked the link then you can go to the page
                if ( !this.classList.contains("active") ) {
                    e.preventDefault();
                }
                this.classList.toggle("active");
            };

        });

    });     


}

function pageOverlay() {

    var overlay = document.getElementById("loading-overlay");

    if (!overlay) return;
    overlay.classList.add("transition");

    setTimeout( function() {
        overlay.remove();
    }, 500 )

}

window.addEventListener('load', function() {  
    initAccordian();
    initSwiper();
    initTeam();
    initHeader();

    equalHeights(".news-card-holder .card h2");
    equalHeights(".news-card-holder .card p");
    equalHeights(".card-holder .card h2");
    equalHeights(".card-holder .card p");
    equalHeights(".three-block-row .card h2");

    AOS.init();

    // once all loaded remove the loading overlay
    pageOverlay();
});

window.addEventListener("resize", function() {
    equalHeights(".news-card-holder .card h2");
    equalHeights(".news-card-holder .card p");
    equalHeights(".card-holder .card h2");
    equalHeights(".card-holder .card p");
    equalHeights(".three-block-row .card h2");
});